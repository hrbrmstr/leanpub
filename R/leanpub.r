#' Sync working Rmd dir with manuscript dir
#'
#' @export
leanpub_sync <- function() {
  root <- rprojroot::find_rstudio_root_file()
  system(sprintf('rsync -rtlzv --delete -a --include="*.md" --exclude "*" %s/rmd/ %s/manuscript/', root, root))
  system(sprintf('rsync -rtlzv --delete -a %s/rmd/images/ %s/manuscript/images/', root, root))
  for (f in list.files(sprintf("%s/manuscript", root), pattern="*.md", full.names=TRUE)) {
    system(sprintf("perl -p -i -e 's|^[[:space:][:alnum:]]*<img src=\"([[:alnum:]\\-/\\.]+)\" width=\"[[:alnum:][:space:]]+\" alt=\"([[:alnum:][:space:]\\-\\./,_]+)\".*|![\\2](\\1)|g' %s", f))
  }
}
